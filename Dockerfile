FROM frolvlad/alpine-bash:latest
WORKDIR /sunflower
COPY archive.sh generate.sh ./
ENTRYPOINT ["/bin/bash", "-c", "/sunflower/generate.sh &> /dev/null"]